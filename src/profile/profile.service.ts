import { Injectable, HttpService, BadRequestException } from '@nestjs/common';
import { AddProfileDto } from './dto/profile.dto';

@Injectable()
export class ProfileService {
  constructor(private readonly httpService: HttpService) {}

  async addProfile(addProfileDto: AddProfileDto) {
    const business_user_id =
      addProfileDto.session_variables['x-hasura-user-id'];
    // console.log(addProfileDto.input.profileInput);

    const query = `
    mutation addProfile(
      $businessName: String!, 
      $address: String!, 
      $locality: String!,
      $webAddress: String!,
      $logoURL: String!
      $description: String!
      $slug: String!
      $formURL: String!
      ){
      insert_profile_one(object: {
        businessName: $businessName, 
        address: $address, 
        locality: $locality,
        webAddress: $webAddress,
        logoURL: $logoURL,
        description: $description,
        slug: $slug,
        formURL: $formURL
        business_user_id: $business_user_id
      }){
        businessName
        address
        locality
        webAddress
        logoURL
        description
        slug
        formURL
      }
    }
    `;

    const variables = {
      businessName: addProfileDto.input.profileInput.businessName,
      address: addProfileDto.input.profileInput.address,
      locality: addProfileDto.input.profileInput.locality,
      webAddress: addProfileDto.input.profileInput.webAddress,
      logoURL: addProfileDto.input.profileInput.logoURL,
      description: addProfileDto.input.profileInput.description,
      slug: addProfileDto.input.profileInput.slug,
      formURL: addProfileDto.input.profileInput.formURL,
      business_user_id: business_user_id,
    };

    const data = this.httpService.post(
      process.env.HASURA_GRAPHQL_ENDPOINT,
      {
        query,
        variables,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'x-hasura-admin-secret': 'irhyxfdluzcpipftwyxkdragnzlsrlqj',
        },
      },
    );

    const newProfile = new Promise((resolve, reject) => {
      data.subscribe(
        async response => {
          resolve(response.data.data['insert_profile_one']);
        },
        error => {
          reject(error);
        },
      );
    });

    const response: any = await newProfile
      .then(async response => {
        if (response == 'Email already exists') {
          throw new BadRequestException('Email already exists');
        }
        return response;
      })
      .catch(error => {
        return {
          error: true,
          message: error,
        };
      });

    return response;
  }
}
