export class AddProfileDto {
  session_variables: {
    'x-hasura-user-id': string;
  };
  input: {
    profileInput: {
      businessName: string;
      address: string;
      locality: string;
      webAddress: string;
      logoURL: string;
      description: string;
      slug: string;
      formURL: string;
    };
  };
}
