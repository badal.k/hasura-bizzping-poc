import { Controller, Post, Body } from '@nestjs/common';
import { AddProfileDto } from './dto/profile.dto';
import { ProfileService } from './profile.service';

@Controller('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}
  @Post()
  async addProfile(@Body() addProfileDto: AddProfileDto) {
    return await this.profileService.addProfile(addProfileDto);
  }
}
