import { Controller, Post, Body, Request } from '@nestjs/common';
import { Credentials, SignupDTO } from './dto/auth.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signin')
  async login(@Body() credentials: Credentials, @Request() headers: any) {
    console.log(headers);
    return await this.authService.login(credentials);
  }

  @Post('signup')
  async signup(@Body() signupDTO: SignupDTO) {
    return await this.authService.signup(signupDTO);
  }
}
