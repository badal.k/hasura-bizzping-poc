export class Credentials {
  'input': {
    authInput: {
      email: string;
      password: string;
    };
  };
}

export class SignupDTO{
  'input': {
    signupInput: {
      name: string,
      email: string,
      mobile: string,
      password: string,
      role: string
    }
  }
}
