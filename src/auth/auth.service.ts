import {
  Injectable,
  HttpService,
  UnauthorizedException,
  BadRequestException,
} from '@nestjs/common';
import { generateToken, verifyToken } from './jwt/index';
import { Credentials, SignupDTO } from './dto/auth.dto';

@Injectable()
export class AuthService {
  constructor(private httpService: HttpService) {}

  async signup(signupDTO: SignupDTO) {
    const query = `
    mutation addUser($name: String!, 
      $email: String!, 
      $mobile: String!,
      $password: String!,
      $role: String!){
      insert_users_one(object: {
        name: $name,
        email: $email,
        mobile: $mobile
        password: $password
        role: $role
      }){
        id
        name
        email
        mobile
        role
      }
    }
    `;

    const variables = {
      name: signupDTO.input.signupInput.name,
      email: signupDTO.input.signupInput.email,
      mobile: signupDTO.input.signupInput.mobile,
      password: signupDTO.input.signupInput.password,
      role: signupDTO.input.signupInput.role,
    };

    const data = this.httpService.post(
      process.env.HASURA_GRAPHQL_ENDPOINT,
      {
        query,
        variables,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'x-hasura-admin-secret': 'irhyxfdluzcpipftwyxkdragnzlsrlqj',
        },
      },
    );

    const newUser = new Promise((resolve, reject) => {
      data.subscribe(
        async response => {
          if (response.data.errors) {
            if (
              response.data.errors[0]['message'] ===
              'Uniqueness violation. duplicate key value violates unique constraint "users_email_key"'
            ) {
              reject('Email already exists');
            }
          } else {
            resolve(response.data.data['insert_users_one']);
          }
        },
        error => {
          reject(error);
        },
      );
    });

    const business_user_id = await this.getBusinessUserId(newUser);

    const response: any = await newUser
      .then(async response => {
        console.log(typeof response);
        if (response == 'Email already exists') {
          throw new BadRequestException('Email already exists');
        }
        return {
          accessToken: await generateToken(
            'accessToken',
            {
              id: newUser['id'],
              email: newUser['email'],
              role: newUser['role'],
            },
            business_user_id,
          ),
        };
      })
      .catch(error => {
        return {
          error: true,
          message: error,
        };
      });

    if (response.error) {
      throw new UnauthorizedException(response.message);
    }
    return response;
  }

  async login(credentials: Credentials) {
    const query = `
        query users_aggregate($email: String!) {
          users_aggregate(where: {email: {_eq: $email}}) {
            nodes {
              id
              email
              password
              role
              profile_id
            }
          }
        }
        `;

    const variables = {
      email: credentials.input.authInput.email,
    };

    const data = this.httpService.post(
      process.env.HASURA_GRAPHQL_ENDPOINT,
      {
        query,
        variables,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'x-hasura-admin-secret': 'irhyxfdluzcpipftwyxkdragnzlsrlqj',
        },
      },
    );

    let user = [];
    user = await new Promise((resolve, reject) => {
      data.subscribe(
        async response => {
          resolve(response.data.data['users_aggregate']['nodes']);
        },
        error => {
          reject(error);
        },
      );
    });

    const business_user_id = await this.getBusinessUserId(user[0]);

    if (
      user.length > 0 &&
      user[0]['email'] === credentials.input.authInput.email &&
      user[0]['password'] === credentials.input.authInput.password
    )
      return {
        accessToken: await generateToken(
          'accessToken',
          {
            id: user[0]['id'],
            email: credentials.input.authInput.email,
            role: user[0]['role'],
          },
          business_user_id,
        ),
      };

    throw new UnauthorizedException('Invalid credentials');
  }

  async getBusinessUserId(user) {
    console.log(user);
    if (user['role'] == 'business_user') {
      return user['id'];
    }

    const query = `
    query users_aggregate($profile_id: uuid!) {
      users_aggregate(where: 
        {
          _and: 
          [
            {
              profile_id: {_eq: $profile_id}, 
              role: {_eq: "business_user"}
            }
          ]
        }) {
        aggregate {
          count
        }
        nodes {
          id
        }
      }
    }
        `;

    console.log('user', user);

    const variables = {
      profile_id: user['profile_id'],
    };

    const data = this.httpService.post(
      process.env.HASURA_GRAPHQL_ENDPOINT,
      {
        query,
        variables,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'x-hasura-admin-secret': 'irhyxfdluzcpipftwyxkdragnzlsrlqj',
        },
      },
    );

    let business_user = [];
    business_user = await new Promise((resolve, reject) => {
      data.subscribe(
        async response => {
          if (response.data.errors) {
            console.log('response', response.data.errors);
          } else {
            console.log('response', response.data);
            resolve(response.data.data['users_aggregate']['nodes']);
          }
        },
        error => {
          console.error('Error....');
          reject(error);
        },
      );
    });

    return business_user[0]['id'];
  }
}
