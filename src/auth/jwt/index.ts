import { sign, verify } from 'jsonwebtoken';

type TokenType = 'accessToken';

const common = {
  accessToken: {
    privateKey: process.env.JWT_TOKEN_KEY,
    signOptions: {
      expiresIn: '30d',
    },
  },
};

export const generateToken = async (
  type: TokenType,
  user,
  business_user_id,
): Promise<string> => {
  return await sign(
    {
      'https://hasura.io/jwt/claims': {
        id: user['id'],
        email: user['email'],
        'x-hasura-user-id': user['id'],
        'x-hasura-allowed-roles': [user['role']],
        'x-hasura-default-role': user['role'],
        'x-hasura-role': user['role'],
        'x-hasura-business-user-id': business_user_id,
      },
    },
    common[type].privateKey,
    {
      algorithm: 'HS256',
      expiresIn: common[type].signOptions.expiresIn, // 15m
    },
  );
};

export const verifyToken = async (token: string, type: TokenType) => {
  await verify(token, common[type].privateKey, async (err, data) => {
    if (err) {
      throw new Error('Authentication token is invalid, please try again.');
    }
  });

  return 'currentUser';
};
